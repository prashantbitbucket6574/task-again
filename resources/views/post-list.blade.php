@extends('layouts.app')
@php
    $user_permissions = auth()->user()->permissions();
@endphp
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    @if(in_array('can_create_post', $user_permissions))
                        <a href="{{ route('posts.create') }}" class="btn btn-success">Add Post </a>
                    @else
                        Post Lists
                    @endif
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <table class="table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>User Name</th>
                                <th>Title</th>
                                <th>Description</th>
                                <th>Image</th>
                                <th>Thumbnail Image</th>
                                <th>Created At</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                @if(count($posts) > 0)
                                    @foreach($posts as $key => $post)
                                        <td>{{ $loop->index+1 }}</td>
                                        <td>{{ $post->user->name }}</td>
                                        <td>{{ $post->title }}</td>
                                        <td>{{ $post->description }}</td>
                                        <td><a href="{{ asset($post->featured_image) }}" target="_blank">view</a></td>
                                        <td><a href="{{ asset($post->thumbnail_image) }}" target="_blank">view</a></td>
                                        <td>{{ $post->created_at }}</td>
                                        <td>
                                            <div class="dropdown">
                                              <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Action
                                              <span class="caret"></span></button>
                                              <ul class="dropdown-menu">
                                                @if(in_array('can_edit_post', $user_permissions))
                                                    <li><a href="{{ route('posts.edit', $post->slug) }}">Edit Post</a></li>
                                                @endif

                                                @if(in_array('can_delete_post', $user_permissions))
                                                    <li>
                                                        <form action="{{ route('posts.destroy', $post->slug) }}" method="post">
                                                            @method('DELETE')
                                                            @csrf
                                                            <input type="submit" name="submit">
                                                        </form>
                                                    </li>
                                                @endif

                                                @if(in_array('can_view_post', $user_permissions))
                                                    <li><a href="{{ route('posts.show', $post->slug) }}">View Detail</a></li>
                                                @endif
                                              </ul>
                                            </div>
                                        </td>
                                    @endforeach
                                @endif
                            </tr>
                        </tbody>
                    </table>
                    {{$posts->links()}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
