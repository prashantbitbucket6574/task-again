@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <a href="{{ route('posts.index') }}" class="btn btn-success">See Post</a>
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div>Post Owner: {{ $post->user->name }}</div>
                    <div>Post Title: {{ $post->title }}</div>
                    <div>Post Description: {{ $post->description }}</div>
                    <div>Post Created At: {{ $post->created_at }}</div>
                    <div>
                        Post Image:- 
                        <div><img src="{{ asset($post->featured_image) }}" class="rounded float-left" alt="..."></div> 
                    </div><br>
                    <div>
                        Post Thumbnail Image:- 
                        <div><img src="{{ asset($post->thumbnail_image) }}" class="rounded float-left" alt="..."></div> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
