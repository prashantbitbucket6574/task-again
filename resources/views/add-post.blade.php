@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    Add Post
                </div>

                <div class="card-body">
                    @if(session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    @if ($errors->any())
                        @foreach ($errors->all() as $error)
                            <div class="alert alert-danger" role="alert">
                                {{$error}}
                            </div>
                        @endforeach
                     @endif
                    <form class="form-horizontal" action="{{ route('posts.store') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="email">Title:</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="email" placeholder="Enter title of post" name="title" value="{{ old('title') }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="pwd">Description:</label>
                            <div class="col-sm-10">
                                <textarea class="form-control" name="description" placeholder="Enter Texts to post">{{old('description')}}</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-6" for="email">Tags (Enter space among tags for multiple):</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="email" placeholder="Enter tags e.g. hello folks" name="tags" value="{{old('tags')}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="pwd">Featured Image:</label>
                            <div class="col-sm-10">
                                <input type="file" class="form-control" name="featured_image">
                            </div>
                        </div>
                        <div class="form-group">        
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                      </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
