<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
	/**
	 * The policy mappings for the application.
	 *
	 * @var array
	 */
	protected $policies = [
		// 'App\Model' => 'App\Policies\ModelPolicy',
	];

	/**
	 * Register any authentication / authorization services.
	 *
	 * @return void
	 */
	public function boot()
	{
		$this->registerPolicies();


		Gate::define('can_create_post', function ($user) {
			return $user->role->hasPermission('can_create_post');
		});

		Gate::define('can_delete_post', function ($user) {
			return $user->role->hasPermission('can_delete_post');
		});

		Gate::define('can_edit_post', function ($user) {
			return $user->role->hasPermission('can_edit_post');
		});

		Gate::define('can_view_post', function ($user) {
			return $user->role->hasPermission('can_view_post');
		});

	}
}
