<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model
{
	use SoftDeletes;
    public $timestamps = true;
    protected $table = 'posts';

    public function user(){
    	return $this->belongsTo(User::class);
    }

    public function tags(){
    	return $this->hasMany(Tag::class, 'post_id', 'id');
    }
}