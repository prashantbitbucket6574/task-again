<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    public $timestamps = false;
    protected $table = 'roles';

    public function permissions()
    {
        return $this->belongsToMany('App\Permission');
    }

    public function hasPermission($perm)
    {
        $user_permissions = $this->permissions
        						->pluck("type")
        						->toArray();

        return in_array($perm, $user_permissions);
    }
}
