<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Str;

use Auth;
use App\Tag;
use App\Post;

class PostController extends Controller
{
	public function __construct(){
		$this->middleware('can:can_create_post')
				->only(['create' ,'store']);
		$this->middleware('can:can_view_post')
				->only(['index', 'show']);
		$this->middleware('can:can_edit_post')
				->only(['edit' ,'update']);
		$this->middleware('can:can_delete_post')
				->only(['destroy']);
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		// dd(auth()->user()->role->hasPermission("can_delete_post"));
		$posts = Post::orderBy('id', 'desc')->paginate(6);
		return view('post-list', compact('posts'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		return view('add-post');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{   

		$this->validate($request, [
			'title' => 'required',
			'description' => 'required',
			'tags' => 'required',
			'featured_image' => 'required|mimes:jpg,jpeg,png',
		],[
			'title.required' => 'Title of the post is required.',
			'description.required' => 'Description of the post is required.',
			'tags.required' => 'Please enter at least one tag for the post.',
			'featured_image.required' => 'Featured image field is required.',
		]);

		$img_path = '';
		if($request->hasfile('featured_image')){
			$img_path = 'storage/'.$request->file('featured_image')->store('post-imgs/'. date('Y/m/d'));
		}
		// dd($request);
		$str = str_shuffle($request->title).'-'.time();
		
		$post = new Post();
		$post->user_id = Auth::user()->id;
		$post->title = $request->title;
		$post->slug = Str::slug($str);
		$post->description = $request->description;
		if($img_path != null){
			$post->featured_image = $img_path;
		}
		$post->thumbnail_image = $img_path;
		$post->save();

		$tags = explode(' ', $request->tags);
		foreach ($tags as $key => $tag) {
			$query = new Tag();
			$query->user_id = Auth::user()->id;
			$query->post_id = $post->id;
			$query->tag = $tag;
			$query->save();
		}

		return redirect()->route('posts.index')->with('status', 'Post Added successfully.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($slug)
	{
		$post = Post::where('slug', $slug)->with('tags', 'user')->first();
		// dd($post);
		return view('post-show', compact('post'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($slug)
	{
		$post = Post::where('slug', $slug)->with('tags')->first();
		// dd($post);
		return view('post-edit', compact('post'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $slug)
	{
		$this->validate($request, [
			'title' => 'required',
			'description' => 'required',
			'tags' => 'required',
			// 'featured_image' => 'required|mimes:jpg,jpeg,png',
		],[
			'title.required' => 'Title of the post is required.',
			'description.required' => 'Description of the post is required.',
			'tags.required' => 'Please enter at least one tag for the post.',
			// 'featured_image.required' => 'Featured image field is required.',
		]);

		$img_path = '';
		if($request->hasfile('featured_image')){
			$img_path = 'storage/'.$request->file('featured_image')->store('post-imgs/'. date('Y/m/d'));
		}
		// dd($request);
		$str = str_shuffle($request->title).'-'.time();
		
		$post = Post::where('slug', $slug)->first();
		if($post == null){
			return redirect()->back()->with('error', 'Post does not exists.');
		}
		$post->user_id = Auth::user()->id;
		$post->title = $request->title;
		$post->slug = Str::slug($str);
		$post->description = $request->description;
		if($img_path != null){
			$post->featured_image = $img_path;
		}
		$post->thumbnail_image = $img_path;
		$post->save();

		$tags = explode(' ', $request->tags);
		Tag::where('post_id', $post->id)->delete();
		foreach ($tags as $key => $tag) {
			$query = new Tag();
			$query->user_id = Auth::user()->id;
			$query->post_id = $post->id;
			$query->tag = $tag;
			$query->save();
		}

		return redirect()->route('posts.index')->with('status', 'Post updated successfully.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($slug)
	{
		Post::where('slug', $slug)->delete();
		return redirect()->route('posts.index')->with('status', 'Post deleted successfully.');
	}
}
