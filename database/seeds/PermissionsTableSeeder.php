<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('permissions')->insert([
            'name' => 'Can Create Post',
            'type' => 'can_create_post',
            'created_at' => date('Y-m-d H:i:s'),
        ]);
        DB::table('permissions')->insert([
            'name' => 'Can Edit Post',
            'type' => 'can_edit_post',
            'created_at' => date('Y-m-d H:i:s'),
        ]);
        DB::table('permissions')->insert([
            'name' => 'Can Delete Post',
            'type' => 'can_delete_post',
            'created_at' => date('Y-m-d H:i:s'),
        ]);
        DB::table('permissions')->insert([
            'name' => 'Can View Post',
            'type' => 'can_view_post',
            'created_at' => date('Y-m-d H:i:s'),
        ]);
    }
}
