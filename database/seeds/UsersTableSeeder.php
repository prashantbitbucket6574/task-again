<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Super Admin',
            'email' => 'superadmin@gmail.com',
            'password' => Hash::make('password'),
            'role_id' => 1,
        ]);
        DB::table('users')->insert([
            'name' => 'Editor',
            'email' => 'editor@gmail.com',
            'password' => Hash::make('password'),
            'role_id' => 2,
        ]);
        DB::table('users')->insert([
            'name' => 'Reader',
            'email' => 'reader@gmail.com',
            'password' => Hash::make('password'),
            'role_id' => 3,
        ]);
    }
}
