<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            'role_type' => 'Super Admin',
            'role_name' => 'Super Admin',
            'created_at' => date('Y-m-d H:i:s'),
        ]);
        DB::table('roles')->insert([
            'role_type' => 'Editor',
            'role_name' => 'Editor',
            'created_at' => date('Y-m-d H:i:s'),
        ]);
        DB::table('roles')->insert([
            'role_type' => 'Reader',
            'role_name' => 'Reader',
            'created_at' => date('Y-m-d H:i:s'),
        ]);
    }
}
